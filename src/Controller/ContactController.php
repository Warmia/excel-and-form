<?php

namespace App\Controller;



use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\HttpFoundation\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Intl\Intl;


class ContactController extends AbstractController
{


    /**
     * @Route("/", name="contact")
     */
    public function contact(Request $request,$id=1)
    {
        \Locale::setDefault('pl');
        $defaultData = ['Wiadomość' => 'Podaj dane'];
        $form = $this-> createFormBuilder($defaultData)
            ->add('NazwaOdbiorcy',TextType::class)
            ->add('TytulWiadomosci',TextType::class)
            ->add('TrescWiadomosci',TextareaType::class)
            ->add('CzasNaOdpowiedz',DateTimeType::class,
                array('data' => new \DateTime('now')
                ))
            ->add('send',SubmitType::class)
            ->getForm();
        $form->handleRequest($request);

        $spreadsheet = new Spreadsheet();


        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Info");
        if($form->isSubmitted() && $form->isValid())
        {
            $id = $request->get('id');
            if($id==null)
            {
                $id =1;
            }
            else
            {
                $id = $request->get('id');
            }

            $receiver = $form["NazwaOdbiorcy"]-> getData();
            $title = $form["TytulWiadomosci"]-> getData();
            $text = $form["TrescWiadomosci"]-> getData();
            $time = $form["CzasNaOdpowiedz"]-> getData();

            $sheet->setCellValue('A1', $receiver);
            $sheet->setCellValue('B1', $title);
            $sheet->setCellValue('C1', $text);
            $sheet->setCellValue('D1', $time);



            $writer = new Xlsx($spreadsheet);


            $excelFilepath =   'Excel' . $id . ' ' . $receiver . ' ' . $title. '.xlsx';

            $id++;

            $writer->save($excelFilepath);

            return $this->redirectToRoute('contact',['id' => $id]);
        }


        return $this->render('contact/contact.html.twig', [
                     'our_form' => $form->createView(),
                  ]);
    }
}
